<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 12/06/2017
 * Time: 11:00
 */

namespace Unit;

use App\Dentist;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Tests\TestCase;

class DentistTest extends TestCase
{
    /** @test */
    public function it_mass_assigned_creatable()
    {
        $dentist = Dentist::create([
            'name' => 'Jorge'
        ]);

        $this->assertEquals('Jorge', $dentist->name);
    }

    /** @test */
    public function it_have_appointment_relationship()
    {
        $dentist = Dentist::create([
            'name' => 'Jorge'
        ]);

        $this->assertInstanceOf(HasMany::class, $dentist->appointments());
    }

    /** @test */
    public function it_get_appointments_related()
    {
        $dentist = Dentist::create([
            'name' => 'Jorge'
        ]);

        $this->assertInstanceOf(Collection::class, $dentist->appointments);
    }
}
