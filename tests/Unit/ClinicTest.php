<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 12/06/2017
 * Time: 10:47
 */

namespace Unit;

use App\Clinic;
use Tests\TestCase;

class ClinicTest extends TestCase
{
    /** @test */
    public function it_mass_assigned_creatable()
    {
        $clinic = Clinic::create([
            'name' => 'Matriz'
        ]);

        $this->assertEquals('Matriz', $clinic->name);
    }
}
