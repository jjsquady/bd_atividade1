<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 12/06/2017
 * Time: 11:09
 */

namespace Unit;

use App\Appointment;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Tests\TestCase;

class AppointmentTest extends TestCase
{
    public function createModel()
    {
        return Appointment::create([
            'dentist_id' => 1,
            'clinic_id' => 1,
            'week_days' => implode('-', ['Seg', 'Qua', 'Sex']),
            'start_time' => '08:00',
            'end_time' => '12:00'
        ]);
    }

    /** @test */
    public function it_mass_assigned_creatable()
    {
        $appointment = $this->createModel();

        $this->assertInstanceOf(Appointment::class, $appointment);
    }

    /** @test */
    public function it_have_a_clinic_relationship()
    {
        $appointment = $this->createModel();

        $this->assertInstanceOf(BelongsTo::class, $appointment->clinic());
    }

    /** @test */
    public function it_have_dentist_relationship()
    {
        $appointment = $this->createModel();

        $this->assertInstanceOf(BelongsTo::class, $appointment->dentist());
    }

    /** @test */
    public function it_mutates_weekdays_array()
    {
        $appointment = Appointment::create([
            'dentist_id' => 1,
            'clinic_id' => 1,
            'week_days' => ['Seg', 'Qua', 'Sex'],
            'start_time' => '08:00',
            'end_time' => '12:00'
        ]);

        $this->assertEquals('Seg-Qua-Sex', $appointment->week_days);
    }
}
