<?php

namespace Tests;

use App\Appointment;
use App\Clinic;
use App\Dentist;

/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 12/06/2017
 * Time: 14:37
 */
trait AppointmentsSetup
{
    public function createDentist($name)
    {
        return Dentist::create([
            'name' => $name
        ]);
    }

    public function createClinic($name)
    {
        return Clinic::create([
            'name' => $name
        ]);
    }

    public function createAppointment()
    {
        return Appointment::create([
            'week_days' => implode('-', ['Seg, Qua, Sex']),
            'start_time' => '08:00',
            'end_time' => date_create_from_format('H:i', '12:00')
        ]);
    }

    public function createCustomAppointment($weekdays = ['Seg','Ter','Qua','Qui','Sex'], $startTime = '08:00', $endTime = '18:00')
    {
        return Appointment::create([
            'week_days' => implode('-', $weekdays),
            'start_time' => $startTime,
            'end_time' => $endTime
        ]);
    }

    public function getDentistAppointments()
    {
        $matriz = $this->createClinic('Matriz');

        $filial = $this->createClinic('Filial');

        $dentist = $this->createDentist('Jorge');

        $matrizAppointment = $this->createCustomAppointment(
            ['Seg','Qua','Sex'],
            '08:00',
            '13:00'
        );

        $matrizAppointment->clinic()->associate($matriz)->save();
        $matrizAppointment->dentist()->associate($dentist)->save();

        $filialAppointmentA = $this->createCustomAppointment(
            ['Ter','Qui'],
            '08:00',
            '12:00'
        );

        $filialAppointmentB = $this->createCustomAppointment(
            ['Ter','Qui'],
            '14:00',
            '18:00'
        );

        $filialAppointmentA->clinic()->associate($filial)->save();
        $filialAppointmentA->dentist()->associate($dentist)->save();

        $filialAppointmentB->clinic()->associate($filial)->save();
        $filialAppointmentB->dentist()->associate($dentist)->save();

        return Appointment::all([
            'id',
            'dentist_id',
            'clinic_id',
            'week_days',
            'start_time',
            'end_time'
        ]);
    }
}