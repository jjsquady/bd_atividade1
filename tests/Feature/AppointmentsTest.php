<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 12/06/2017
 * Time: 12:59
 */

namespace Feature;

use Illuminate\Support\Facades\Session;
use Tests\AppointmentsSetup;
use Tests\TestCase;

class AppointmentsTest extends TestCase
{
    use AppointmentsSetup;

    /** @test */
    public function it_can_show_list()
    {
        $this->getDentistAppointments();

        $response = $this->get('/appointments');

        $response->assertSee('Dentists Appointments');

        $response->assertViewIs('appointments.index');

        $response->assertViewHas('appointments');

        /* See list */
        $response->assertSee('Jorge');
        $response->assertSee('Matriz');
        $response->assertSee('Filial');
        $response->assertSee('Seg-Qua-Sex');
        $response->assertSee('Ter-Qui');
        $response->assertSee('08:00-13:00');
        $response->assertSee('08:00-12:00');
        $response->assertSee('14:00-18:00');

    }

    /** @test */
    public function it_can_create_a_new_appointment_from_list()
    {
        $response = $this->get('appointments');

        $response->assertSee('Create New Appointment');

    }

    /** @test */
    public function it_can_click_and_see_a_form_to_create_a_new_appointment()
    {
        /* Simulate "click" on Create New Appointment Button-Link */
        $response = $this->get('appointment/new');

        $response->assertViewIs('appointments.create');

        $response->assertSee('<h1>Create new Appointment</h1>');

    }

    /** @test */
    public function it_can_select_a_dentist()
    {
        $this->createDentist('Jorge');

        $response = $this->get('appointment/new');

        $response->assertSeeText('Jorge');
    }

    /** @test */
    public function it_can_select_a_clinic()
    {
        $this->createClinic('Matriz');

        $response = $this->get('appointment/new');

        $response->assertSeeText('Matriz');
    }

    /** @test */
    public function it_can_post_and_validate_appointment()
    {
        Session::start();

        /* only send a empty form */
        $response = $this->call('POST', 'appointments', [
            '_token' => csrf_token()
        ]);

        $response->assertSessionHasErrors([
            'dentist_id',
            'clinic_id',
            'week_days',
            'start_time',
            'end_time'
        ]);
    }

    /** @test */
    public function it_post_and_persist_data()
    {
        Session::start();

        $response = $response = $this->call('POST', 'appointments', [
            '_token' => csrf_token(),
            'dentist_id' => 1,
            'clinic_id' => 1,
            'week_days' => ['Seg', 'Sex'],
            'start_time' => '08:00',
            'end_time' => '13:00'
        ]);

        $this->assertDatabaseHas('appointments', [
            'dentist_id' => 1,
            'clinic_id' => 1,
            'week_days' => 'Seg-Sex',
            'start_time' => '08:00',
            'end_time' => '13:00'
        ]);

        $response->assertRedirect('appointments');
    }

}
