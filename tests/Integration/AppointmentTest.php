<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 12/06/2017
 * Time: 11:28
 */

namespace Integration;

use Tests\AppointmentsSetup;
use Tests\TestCase;

class AppointmentTest extends TestCase
{

    use AppointmentsSetup;

    /** @test */
    public function it_get_dentist()
    {
        $dentist = $this->createDentist('Jorge');

        $appointment = $this->createAppointment();

        $appointment->dentist()->associate($dentist)->save();

        $this->assertEquals([1, 'Jorge'], array($appointment->dentist->id, $appointment->dentist->name));
    }

    /** @test */
    public function it_get_clinic()
    {
        $clinic = $this->createClinic('Matriz');

        $appointment = $this->createAppointment();

        $appointment->clinic()->associate($clinic)->save();

        $this->assertEquals([1, 'Matriz'], array($appointment->clinic->id, $appointment->clinic->name));
    }

    /** @test */
    public function it_get_total_dentist_appointments()
    {
        $appointments = $this->getDentistAppointments();

        $this->assertEquals(3, $appointments->count());
    }

    /** @test */
    public function it_get_dentist_appointments_details()
    {
        $appointments = $this->getDentistAppointments();

        $this->assertEquals([
            [
                'id' => 1,
                'dentist_id' => 1,
                'clinic_id' => 1,
                'week_days' => 'Seg-Qua-Sex',
                'start_time' => '08:00',
                'end_time' => '13:00'
            ],
            [
                'id' => 2,
                'dentist_id' => 1,
                'clinic_id' => 2,
                'week_days' => 'Ter-Qui',
                'start_time' => '08:00',
                'end_time' => '12:00'
            ],
            [
                'id' => 3,
                'dentist_id' => 1,
                'clinic_id' => 2,
                'week_days' => 'Ter-Qui',
                'start_time' => '14:00',
                'end_time' => '18:00'
            ]
        ], $appointments->toArray());
    }
}
