<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 12/06/2017
 * Time: 16:12
 */

namespace App\Support;


class TimeOptions
{
    public static function generate($start = 8, $end = 18)
    {
        $times = [];

        for ($i = $start; $i <= $end; $i++) {
            $times[] = date('H:i', strtotime("$i:00"));
        }

        return $times;
    }

    public static function weekDaysNames()
    {
        return [
            'Seg',
            'Ter',
            'Qua',
            'Qui',
            'Sex'
        ];
    }
}