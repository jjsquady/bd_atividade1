<?php
/**
 * Created by PhpStorm.
 * User: Jorge
 * Date: 13/06/2017
 * Time: 13:12
 */

namespace App\Support;


class FormHelper
{
    public static function hasSelected($oldKey, $value)
    {
        return old($oldKey) == $value ? 'selected' : '';
    }

    public static function hasChecked($oldKey, $value)
    {
        if (is_array(old($oldKey)) && in_array($value, old($oldKey))) {
            return 'checked';
        }

        return old($oldKey) == $value ? 'checked' : '';
    }
}