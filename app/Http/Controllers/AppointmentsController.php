<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Clinic;
use App\Dentist;
use App\Http\Requests\AppointmentFormRequest;
use Illuminate\Http\Request;

class AppointmentsController extends Controller
{
    public function index()
    {
        $appointments = Appointment::with([
            'dentist',
            'clinic'
        ])->orderBy('dentist')->get();

        return view('appointments.index', compact('appointments'));
    }

    public function create()
    {
        $dentists = Dentist::all(['id', 'name']);

        $clinics = Clinic::all(['id', 'name']);

        return view('appointments.create', compact('dentists', 'clinics'));
    }

    public function store(AppointmentFormRequest $request)
    {
        Appointment::create($request->all());

        return redirect()->route('appointment.index');
    }
}
