<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = [
        'dentist_id',
        'clinic_id',
        'week_days',
        'start_time',
        'end_time'
    ];

    public function clinic()
    {
        return $this->belongsTo(Clinic::class);
    }

    public function dentist()
    {
        return $this->belongsTo(Dentist::class);
    }

    public function setWeekDaysAttribute($value)
    {
        if(is_array($value)) {
            $value = implode('-', $value);
        }

        $this->attributes['week_days'] = $value;
    }
}
