@extends('layouts.app')

@inject('timeOptions', "App\Support\TimeOptions")
@inject('formHelper', "App\Support\FormHelper")

@section('content')

    <div class="container">

        <h1>Create new Appointment</h1>

        <hr>

        @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('appointment.store') }}" method="post">

            {{ csrf_field() }}

            <div class="form-group">
                <label for="dentists">Dentista:</label>
                <select name="dentist_id" id="dentists">
                    <option value="" selected>Selecione um Dentista...</option>
                    @foreach($dentists as $dentist)
                        <option value="{{ $dentist->id }}"
                                {{ $formHelper->hasSelected('dentist_id', $dentist->id) }}
                        >{{ $dentist->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="clinics">Clínica:</label>
                <select name="clinic_id" id="clinics">
                    <option value="" selected>Selecione uma Clínica...</option>
                    @foreach($clinics as $clinic)
                        <option value="{{ $clinic->id }}"
                                {{ $formHelper->hasSelected('clinic_id', $clinic->id) }}
                        >{{ $clinic->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="week_days">Dias da semana:</label>
                <br />
                @foreach($timeOptions->weekDaysNames() as $weekDay)
                    <label>
                        <input name="week_days[]"
                               value="{{ $weekDay }}"
                               type="checkbox"
                               {{ $formHelper->hasChecked('week_days', $weekDay) }}
                        > {{ $weekDay }}
                    </label>
                @endforeach
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="start_time">Início:</label>
                        <select name="start_time" id="start_time">
                            <option value="">-</option>
                            @foreach($timeOptions->generate() as $time)
                                <option value="{{ $time }}" {{ $formHelper->hasSelected('start_time', $time) }}>{{ $time }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label for="end_time">Fim:</label>
                        <select name="end_time" id="end_time">
                            <option value="">-</option>
                            @foreach($timeOptions->generate() as $time)
                                <option value="{{ $time }}" {{ $formHelper->hasSelected('end_time', $time) }}>{{ $time }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Create</button>

        </form>

    </div>

@endsection