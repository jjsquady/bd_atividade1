@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>Dentists Appointments</h1>

        <div class="col-md-4">
            <a href="{{ route('appointment.create') }}" class="btn btn-block btn-primary">
                Create New Appointment
            </a>
        </div>

        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                <th>Nome</th>
                <th>Clínica</th>
                <th>Horário de Atendimento</th>
                </thead>
                <tbody>
                @foreach($appointments as $appointment)
                    <tr>
                        <td>{{ $appointment->dentist->name }}</td>
                        <td>{{ $appointment->clinic->name }}</td>
                        <td>
                            {{ $appointment->week_days }}: {{ $appointment->start_time }}-{{ $appointment->end_time }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection