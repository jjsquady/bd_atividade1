# Atividade 01

Build: [![CircleCI](https://circleci.com/bb/jjsquady/bd_atividade1.svg?style=svg)](https://circleci.com/bb/jjsquady/bd_atividade1)  [ ![Codeship Status for jjsquady/bd_atividade1](https://app.codeship.com/projects/6ce3c020-3dc5-0135-3d30-16430db66652/status?branch=master)](https://app.codeship.com/projects/229284)

### BlueDental

#### Escopo:

Um dentista trabalha em duas clínicas, Matriz e Filial, na matriz ele trabalha segunda, quarta e sexta, das 08h às 13h e na Filial terça e quinta das 08h às 18h com um intervalo de almoço de 12h às 14h. 

Com os dados acima, construa um cadastro que permita armazenar essas informações e que sejam exibidas as uma listagem os seguintes dados Nome do Dentista, Clínica, horário de atendimento.

#### Resolução

Projeto em Laravel 5 (5.4.X), inclui TDD e a resolução do escopo.

#### Requisitos

* Requisitos do [laravel 5.4](https://laravel.com/docs/5.4#server-requirements)
* Composer
* PHPUnit (para os testes)

## Instalação

```
git clone https://bitbucket.org/jjsquady/bd_atividade1.git
```

##### Na pasta do projeto (via terminal) faça:

```
$ composer install
$ cp .env.example .env
$ php artisan migrate:refresh
$ php artisan db:seed
$ php artisan serve
```

Abra o endereço `http://localhost:8000/appointments` no navegador.

### Testes

```
phpunit tests/
```

