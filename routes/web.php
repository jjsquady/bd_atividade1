<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('appointment.index');
});

Route::get('appointments', 'AppointmentsController@index')->name('appointment.index');
Route::get('appointment/new', 'AppointmentsController@create')->name('appointment.create');
Route::post('appointments', 'AppointmentsController@store')->name('appointment.store');