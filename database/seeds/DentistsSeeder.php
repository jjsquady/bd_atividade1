<?php

use App\Dentist;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DentistsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::getSchemaBuilder()->hasTable('dentists')) {

            DB::table('dentists')->truncate();

            factory(Dentist::class, 8)->create();
        }
    }
}
