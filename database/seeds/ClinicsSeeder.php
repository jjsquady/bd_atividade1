<?php

use App\Clinic;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClinicsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::getSchemaBuilder()->hasTable('clinics')) {

            DB::table('clinics')->truncate();

            Clinic::create([
                'name' => 'Matriz'
            ]);

            Clinic::create([
                'name' => 'Filial'
            ]);
        }
    }
}
